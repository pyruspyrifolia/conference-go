import requests

from .models import ConferenceVO

def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = response.json()

    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            #what data am i caching on, identifying
            import_href=conference["href"],
            #what you're updating
            defaults={"name": conference["name"], "description":conference["description"]}
        )
