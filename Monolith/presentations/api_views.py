from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Presentation
from events.models import Conference
import json

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title"
    ]
    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET","PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentations = Presentation.objects.get(id=id)
        return JsonResponse(
        {"presentation": presentations},
        encoder=PresentationDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:

                content["conference"] = Conference.objects.get(name=content["conference"])
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference name"},
                status=400,
            )

        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET","POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        # Get the conference object and put it in the content dict
        # in the following line we assign Conference property

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference Abbreviation"},
                status=400,
            )
    presentation = Presentation.create(**content)
    return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )
